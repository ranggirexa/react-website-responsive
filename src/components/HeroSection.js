import React from 'react'
import '../App.css'
import {Button} from './Button'
import './HeroSection.css'

function HeroSection() {
  return (
    <div className='hero-container'> 
        {/* <video src='../../public/videos/video-2.mp4' autoPlay loop muted /> */}
        <video loop autoPlay muted>
            <source src='/videos/video-2.mp4' type="video/mp4"/>
        </video>
        <h1>ADVENTURE AWAITS</h1>
        <p>what are you waiting for?</p>
        <div className='hero-btns'>
            <Button className='btns' buttonStyle='btn--outline' buttonSize='btn--large'>
                Get Started
            </Button>
            <Button className='btns' buttonStyle='btn--primary' buttonSize='btn--large'>
                Watch Trailer <i className='far fa-play-circle' />
            </Button>
        </div>
    </div>
  )
}

export default HeroSection